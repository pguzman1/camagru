<?php

/**
* This class will be able to handle all database requests.
*/
class Database extends PDO
{
  public $host       = 'localhost';
  public $username   = 'root';
  public $password   = 'root';
  public $dbname     = 'test';
  public $conn;

  function __construct()
  {
    try
    {
      parent::__construct('mysql:host='.$this->host.';', $this->username, $this->password);
      $this->setAttribute(self::ATTR_ERRMODE,self::ERRMODE_EXCEPTION);
    }
    catch (PDOException $e)
    {
      echo $e->getMessage();
      die();
      return false;
    }
  }

  function myquery($sql)
  {
    $a = $this->prepare($sql);
    $a->execute();
    return $a;
  }

  function get_user_key_by_pseudo($pseudo)
  {
    $sql = "SELECT keyconf FROM USERS WHERE pseudo='$pseudo'";
    return $this->get_result($sql);
  }

  function get_user_pseudo_by_key($key)
  {
    $sql = "SELECT pseudo FROM USERS WHERE keyconf='$key'";
    return $this->get_result($sql);
  }

  function get_user_id_by_key($key)
  {
    $sql = "SELECT id FROM USERS WHERE keyconf='$key'";
    return $this->get_result($sql);
  }

  function get_user_id_by_pseudo($key)
  {
    $sql = "SELECT id FROM USERS WHERE pseudo='$key'";
    return $this->get_result($sql);
  }

  function get_picture_id_by_path($path)
  {
    $sql = "SELECT id FROM PICTURES WHERE path_pic='$path'";
    return $this->get_result($sql);
  }


  function get_pictures_from($pseudo)
  {
    $a = $this->get_user_id_by_pseudo($pseudo);
    $sql = "SELECT path_pic FROM PICTURES WHERE user_id='$a'";
    $a = $this->myquery($sql);
    $result = $a->fetchAll(PDO::FETCH_COLUMN, 0);
    return $result;
  }

  function get_all_pictures()
  {
    $sql = "SELECT path_pic FROM PICTURES";
    $a = $this->myquery($sql);
    $result = $a->fetchAll(PDO::FETCH_COLUMN, 0);
    return $result;
  }

  function get_result($sql)
  {
    $a = $this->myquery($sql);
    $result = $a->fetch(PDO::FETCH_COLUMN, 0);
    return ($result);
  }

  function activateAccount($pseudo)
  {
    setcookie("keyy", ' ', time());
    $sql = "UPDATE USERS SET validated=1 WHERE pseudo='$pseudo'";
    $this->myquery($sql);
  }

  function sendmail($mail, $keyy)
  {
    $to = $mail;
    $subject = "This is you confirmation key";
    $lien = 'http://localhost:8080/index.php?keyy=' . $keyy;
    $txt = "Hello world!  go to this site to activate your account <a href='$lien'>Zelda</a>\n\n";
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= "From: webmaster@camagru.com";
    mail($to,$subject,$txt,$headers);
  }

  function sendmailpass($mail,$newpass)
  {
    $to = $mail;
    $subject = "This is you new password";
    $txt = "this is you new password  ='$newpass'";
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= "From: webmaster@camagru.com";
    mail($to,$subject,$txt,$headers);
  }

  function allmails()
  {
    $sql = "SELECT mail FROM USERS";
    $a = $this->myquery($sql);
    $result = $a->fetchAll(PDO::FETCH_COLUMN, 0);
    return ($result);
  }

  function change_password($mail, $pseudo, $newpasswd)
  {
    $sql = "SELECT id FROM USERS WHERE mail='$mail'";
    $a = $this->get_result($sql);
    if ($a)
    {
      $sql = "UPDATE USERS SET passwod='$newpasswd' WHERE mail='$mail' AND pseudo='$pseudo'";
      $this->myquery($sql);
      echo"
      <script>
      alert('MDP CHANGED');
      </script>
      ";
    }
    else {
      echo"
      <script>
      alert('NOT GOOOOOOD');
      </script>
      ";
    }

  }

  function number_of_rows($sql)
  {
    $a = $this->myquery($sql);
    $res = $a->fetchAll(PDO::FETCH_COLUMN, 0);
    return count($res);
  }

  function verif_pass($user, $pass)
  {
    $hash = hash('whirlpool', $pass);
    $sql = "SELECT validated FROM USERS WHERE passwod='$hash' AND pseudo='$user'";
    if ($this->number_of_rows($sql) == 1 && in_array($user, $this->allpseudo()) && $this->get_result($sql) == 1)
    return 1;
    else {
      return 0;
    }
  }

  function allpseudo()
  {
    $sql = "SELECT pseudo FROM USERS";
    $a = $this->myquery($sql);
    $result = $a->fetchAll(PDO::FETCH_COLUMN, 0);
    return ($result);
  }

  function get_alpha_array()
  {
    $res = array();
    // $array = array_diff(scandir('upload/alpha'), array('..', '.'));
    $array = scandir('upload/alpha');
    for ($i=0; $i < count($array); $i++) {
      if ($array[$i][0] != '.')
      array_push($res, $array[$i]);
    }
    return $res;
  }

  function add_pic($path, $user_id)
  {
    $this->myquery("INSERT INTO PICTURES (user_id,path_pic, likes) VALUES ('$user_id', '$path', 0)");
  }

  function print_pictures($pictures)
  {
    $left = count($pictures);
    $i = 0;
    while ($pictures[$i])
    {
      if ($i % 4 == 0)
      {
        if ($i != 0)
          echo "</div>";

        echo "
          <div id= 'roow'>
        ";
      }
      $u = $pictures[$i];
      echo "
          <div id= 'boxx' onclick='gotophoto(\"$u\")'  style='content: url($pictures[$i])'>
          </div>
      ";
      $i++;
    }
  }

  function likes($picture)
  {
    $pic_id = $this->get_picture_id_by_path($picture);
    $sql = "SELECT id FROM LIKES WHERE picture_id='$pic_id'";
    $a = $this->myquery($sql);
    $result = $a->fetchAll(PDO::FETCH_COLUMN, 0);
    if (!$result)
      return 0;
    else
      return count($result);
  }

  function print_picture($picture)
  {
    if ($_SESSION['pseudo'])
    {
      $re = "refresh";
    echo "
          <div id='photo_container'>
            <div id='one_photo' style='content: url($picture)'>
            </div>
            <div id='likelike'></div>
            <button id='button_like' type='button' name='button' onclick='likephoto(\"$picture\")'>like me</button>
            <div id='likes'>
            Likes = ".$this->likes($picture)."
            </div>
            <div id='comment_form'>

              Comment:<br />

              <textarea name='comment' id='comment'></textarea><br />
              <button type='button' onclick='process(\"$re\")'>comment</button>
            </div>

          </div>
    ";
    }
  }

              //
              // <div id='comments'>
              // ".$this->print_comments($this->get_picture_id_by_path($picture))."
              // </div>

  function print_comments($picture_id)
  {
    $all_user_id;
    $all_comments;
    $i = 0;
    while($all_user_id[$i])
    {
      echo "

      <h2>$all_user_id[$i]</h2></br>
      <h3>$all_comments[$i]</h3>
      ";
      $i++;
    }

  }

  function print_mini($num_pic, $pictures)
  {
    $i = 0;

    while ($i < 7)
    {
      if ($pictures[$i])
      {
        echo "<div class='photo'";
        if ($num_pic > $i) echo "style='content: url($pictures[$i])'";
        echo ">";
        echo "</div>";
      }
      $i++;
    }
  }

  function alreadyliked($picture_id, $user_id)
  {
    $id = $this->get_picture_id_by_path($picture_id);
    $a = $this->myquery("SELECT id FROM LIKES WHERE picture_id='$id' AND user_id='$user_id'");
    $result = $a->fetchAll(PDO::FETCH_COLUMN, 0);
    return $result;
  }

  function addlike($picture_id, $user_id)
  {
    $id = $this->get_picture_id_by_path($picture_id);
    $this->myquery("INSERT INTO LIKES (picture_id, user_id) VALUES ('$id', '$user_id')");
    $this->myquery("UPDATE PICTURES SET likes=likes+1 WHERE path_pic='$picture_id'");
  }

  function addcomment($user_id, $picture_id, $comment)
  {
    $this->myquery("INSERT INTO COMMENTS (user_id, pictures_id, comment) VALUES ('$user_id', '$picture_id', '$comment')");
  }

  function removelike($picture_id, $user_id)
  {
    $id = $this->get_picture_id_by_path($picture_id);
    $this->myquery("DELETE FROM LIKES WHERE picture_id='$id' AND user_id='$user_id'");
    $this->myquery("UPDATE PICTURES SET likes=likes-1 WHERE path_pic='$picture_id'");
  }
}
?>
