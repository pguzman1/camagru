<?php
include('database.class.php');
include('user.class.php');

$conn = new Database;
$conn->exec("USE mydb");
$alpha_array = $conn->get_alpha_array();
if (!isset($_COOKIE[yes]))
{
  die("GO AWAY, THIS IS NOT FOR YOU");
}
else
{
  $keyy = $_COOKIE[yes];
  $result = $conn->get_user_pseudo_by_key($keyy);
  $result2 = $conn->get_user_id_by_key($keyy);
  if (!$result)
  die("GO AWAY, THIS IS NOT FOR YOU");
  else {
    session_start();
    $super = new User($result, $result2);
    $_SESSION['pseudo'] = $super->getPseudo();
    $_SESSION['id'] = $super->getid();
    $_SESSION['alpha_array'] = serialize($alpha_array);
    $_SESSION['position'] = 0;
    // setcookie('alpha_array', serialize($alpha_array));
    setcookie('position', 0);
    $pictures = $conn->get_pictures_from($_SESSION['pseudo']);
    $num_pic = count($pictures);
  }
}

/*
** TO-DO LIST:
** logout button from everywhere                                                DONE
** upload image and make like if it was just taken by the camera
** oublie de mot de pass                                                        DONE
** Mini-pictures on the side
** Page Galerie: likes under the pictures
**               lots of pages
** Page Photo: likes and comments
**
*/

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>TITLE</title>
  <link rel="stylesheet" href="cama.css" media="screen" title="no title">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <div id="big_container">
    <div id="header_container">
      <div id="header_logo">

      </div>
      <div id="header_middle">

      </div>
      <div id="header_buttons">
        <div id="button_pass">

        </div>
        <div id="button_logout">
          <button id="logout_button" type="button" onclick="logout();" name="button">log out</button>
          <button id="gomain" type="button" name="button" onclick="gomain();" >Go to Main</button>
        </div>
      </div>
    </div>
    <div id="body_container">
      <div id="side_container">
        <div id="photos">
          <?php $conn->print_mini($num_pic, $pictures); ?>
        </div>
        <div id="all_photos">
          <button id="myPhotos_button" type="button" onclick="allmyphotos();" name="button">all your photos</button>
          <button id="myPhotos_button" type="button" onclick="allphotos();" name="button">all the photos</button>
        </div>
      </div>
      <div id="video_container">
        <div id="vide">
        </div>
        <div id="videohtml">
          <div id="videvideo1">
          </div>
          <div id="video_id">
            <video onclick="change_next_effect();" width="320" height="240" id="video"></video>
            <img onclick="change_next_effect();"  id="effect_big_picture" src="upload/alpha/42_trans.png"  />
            <canvas  id="canvas" width="320" height="240"></canvas>
          </div>
          <div id="videvideo2">
            <div id="buttons">
              <div id="buttons_indeed">
                <button onclick="snap();">Snap</button>
                <!-- <button onclick="change_effect();">42fy it!!!</button> -->
                <button onclick="save_pic();">save it!!</button>
              </div>
              <div id="buttons_upload">
                <form action="upload.php" method="post" enctype="multipart/form-data">
                  Select image to upload:
                  <input type="file" name="fileToUpload" id="fileToUpload">
                  <input type="submit" value="Upload Image" name="submit">
                </form>
              </div>
            </div>
            <div id="preimageid">
              <img src="" id='preimage' />
            </div>
          </div>
        </div>
        <div id="vide">

        </div>
      </div>
    </div>
    <div id="feet_container">

    </div>
  </div>
  <script type="text/javascript" src="/scripts2.js"></script>
</body>
</html>
