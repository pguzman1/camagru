<?php
include('database.class.php');
include('user.class.php');

if(isset($_GET['who']))
{
  if ($_GET['who'] == "me")
  {
    session_start();
    $conn = new Database;
    $conn->exec("USE mydb");
    if ($_SESSION['pseudo'])
      $pictures = $conn->get_pictures_from($_SESSION['pseudo']);
    else
      die("Sosomething went wronrong");
  }
  else if ($_GET['who'] == "everyone")
  {
    session_start();
    $conn = new Database;
    $conn->exec("USE mydb");
    if ($_SESSION['pseudo'])
      $pictures = $conn->get_all_pictures();
    else
      die("Sosomething went wronrong");
  }
}
elseif (isset($_GET['one'])) {
  session_start();
  $conn = new Database;
  $conn->exec("USE mydb");
  $picture=$_GET['one'];
}
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>TITLE</title>
  <link rel="stylesheet" href="cama.css" media="screen" title="no title">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <div id="big_container">
    <div id="header_container">
      <div id="header_logo">
      </div>
      <div id="header_middle">
      </div>
      <div id="header_buttons">
        <div id="button_pass">
        </div>
        <div id="button_logout">
          <button id="logout_button" type="button" onclick="logout();" name="button">log out</button>
          <button id="gomain" type="button" name="button" onclick="gomain();" >Go to Main</button>
        </div>
      </div>
    </div>

    <div id="body_container2">
      <?php

        if ($pictures)
          $conn->print_pictures($pictures);
        if($picture)
          $conn->print_picture($picture);
      ?>
    <div id="feet_container">
    </div>
  </div>
  <script type="text/javascript" src="/scripts2.js"></script>
</body>
</html>
