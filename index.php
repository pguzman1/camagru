<?php
include('database.class.php');
$conn = new Database;
if (!isset($_POST['mail']) && !isset($_GET['keyy']) && isset($_GET['start']) && $_GET['start'] == "yes")
{
  $sql = "DROP DATABASE IF EXISTS mydb";
  $conn->myquery($sql);
  $sql = "CREATE DATABASE IF NOT EXISTS mydb";
  $conn->myquery($sql);
  $conn->exec("USE mydb");
  $sql = "CREATE TABLE IF NOT EXISTS USERS (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  pseudo VARCHAR(30) NOT NULL,
  mail VARCHAR(30) NOT NULL,
  passwod VARCHAR (500) NOT NULL,
  keyconf VARCHAR(80) NOT NULL,
  validated INT(2) NOT NULL)";
  $conn->myquery($sql);
  $sql = "CREATE TABLE IF NOT EXISTS PICTURES (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_id INT(6) NOT NULL,
  path_pic VARCHAR(60) NOT NULL,
  likes INT(6) NOT NULL)";
  $conn->myquery($sql);
  $sql = "CREATE TABLE IF NOT EXISTS COMMENTS (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_id int(6) NOT NULL,
  picture_id int(6) NOT NULL,
  comment VARCHAR(300) NOT NULL)";
  $conn->myquery($sql);
  $sql = "CREATE TABLE IF NOT EXISTS LIKES (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  picture_id int(6) NOT NULL,
  user_id int(6) NOT NULL)";
  $conn->myquery($sql);
  $ha = hash('whirlpool', '1234');
  $conn->myquery("INSERT INTO USERS (pseudo, mail, passwod, keyconf, validated) VALUES ('Lucie', 'a@a.a', '$ha', '2234', '1')");
  $conn->myquery("INSERT INTO USERS (pseudo, mail, passwod, keyconf, validated) VALUES ('Pato', 'pguzmanosorio@gmail.com', '$ha', '2534', '1')");
  $conn->myquery("INSERT INTO USERS (pseudo, mail, passwod, keyconf, validated) VALUES ('Sofia', 'b@b.b', '1234', '2', '0')");
  $conn->myquery("INSERT INTO USERS (pseudo, mail, passwod, keyconf, validated) VALUES ('Catalina', 'c@c.c', '1234', '2', '0')");
  $conn->myquery("INSERT INTO USERS (pseudo, mail, passwod, keyconf, validated) VALUES ('Haude', 'hola@hotmail.fr', '1234', '0', '2')");
}
else {
  $conn->exec("USE mydb");
}
if (isset($_POST['mail']))
{
  /*
  ** Create Account
  */

  $pss = hash('whirlpool', $_POST[password]);
  $idd = uniqid();
  $conn->myquery("INSERT INTO USERS (pseudo, mail, passwod, keyconf, validated) VALUES ('$_POST[pseudo]', '$_POST[mail]', '$pss', '$idd', 0)");
  $conn->sendmail($_POST[mail], $idd);
  setcookie("keyy", $_POST[pseudo], time() + 3600);
}
else if (isset($_POST['mail_forgot']))
{
  /*
  ** Forgotten password
  */

  $random_pass = uniqid();
  $pss = hash('whirlpool', $random_pass);
  $conn->change_password($_POST['mail_forgot'], $_POST['pseudo_forgot'], $pss);
  $conn->sendmailpass($_POST['mail_forgot'], $random_pass);
}
else if (isset($_GET['keyy']))
{
  /*
  ** Activate Account
  */

  if ($_GET[keyy] == $conn->get_user_key_by_pseudo($_COOKIE[keyy]))
  $conn->activateAccount($_COOKIE[keyy]);
  echo"
  <script>
  alert('Account activated, congrats!!!!!');
  </script>
  ";
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Bonjour, sign up, sign up</title>
  <link rel="stylesheet" href="/styles.css" media="screen" title="no title" charset="utf-8">
  <script type="text/javascript" src="/scripts.js"></script>
  <script type="text/javascript">
  function start(){
    process();
    process2();
  };
  </script>
</head>
<body onload="start()">
  <div class="firstblock">
  </div>
  <div class="boxbox">
    <div class="firstblock">
    </div>
    <div id="content_form">
      <p style="font-family:'coneria'">
        Bonjour, Hola, Hello.
      </p>
      <div id="boxinside">
        <input type="button" name="name" onclick="gotologin()" value="log in">
        <br>
        <input type="button" name="name" onclick="gotocreateaccount()" value="create account">
        <br>
        <input type="button" name="name" onclick='donotclick()' value="do not click">
        <br>
        <input type="button" name="name" onclick='forgot()' value="forgot password??">
      </div>
    </div>
    <div class="firstblock">
    </div>
  </div>
  <div class="firstblock">
  </div>
</body>
</html>
