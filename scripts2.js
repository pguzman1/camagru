

var video = document.getElementById('video');
var canvas = document.getElementById('canvas');
if (canvas)
  var context = canvas.getContext('2d');

var xmlHttp = createXmlHttpRequestObject();

function createXmlHttpRequestObject()
{
  var xmlHttp;

  if(window.ActiveXObject){
    try{
      xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }catch(e){
      xmlHttp = false;
    }
  }else{
    try{
      xmlHttp = new XMLHttpRequest();
    }catch(e){
      xmlHttp = false;
    }
  }

  if(!xmlHttp)
  alert("Cant create that object !")
  else
  return xmlHttp;
}

function process(data)
{
  var params = "upload=" + data;
  if (data == "refresh")
  {
    data1 = {
      comment : document.getElementById("comment").value,
      pic_path : document.getElementById('one_photo').style.content
    };
    params = "upload=" + JSON.stringify(data1);
  }
  if(xmlHttp.readyState==0 || xmlHttp.readyState==4)
  {
    xmlHttp.open("POST", "log.php",true);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.onreadystatechange = function() {
        if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
        {
            if (data != "next_effect" && data != "save_pic" && data != "novideo")
            {
              if (data != "refresh")
              {
                if (data.substring(data.length - 4, data.length) != ".png")
                  document.getElementById('preimage').src = "dest.png?" + new Date().getTime();
              }
            }
            if (data == "next_effect")
            {
              xmlResponse = xmlHttp.responseXML;
              xmlDocumentElement = xmlResponse.documentElement;
              message = xmlDocumentElement.firstChild.data;
              if (message[0] == '.')
              {
                message = message.substring(1);
                var ar = document.getElementById("effect_big_picture");
                if (!ar)
                  var ar = document.getElementById("effect_small_picture");
                else
                {
                  document.getElementById("effect_big_picture").src = "upload/alpha/"+ message;
                  ar.id = "effect_small_picture";
                }
              }
              else
              {
                var ar = document.getElementById("effect_small_picture");
                if (!ar)
                  document.getElementById("effect_big_picture").src = "upload/alpha/"+ message;
                else
                {
                  document.getElementById("effect_small_picture").src = "upload/alpha/"+ message;
                  ar.id = "effect_big_picture";
                }
              }
            }
            else if (data=="save_pic" || data == "novideo" || data == "refresh")
            {
              xmlResponse = xmlHttp.responseXML;
              xmlDocumentElement = xmlResponse.documentElement;
              message = xmlDocumentElement.firstChild.data;
              alert(message);
            }
            else if (data.substring(data.length - 4, data.length) == ".png")
            {
              xmlResponse = xmlHttp.responseXML;
              xmlDocumentElement = xmlResponse.documentElement;
              message = xmlDocumentElement.firstChild.data;
              alert(message);
              document.getElementById('button_like').innerHTML = message;
              document.getElementById('button_like').style.color = "green";
              document.getElementById('button_like').style.backgroundColor = "#4CAF50";
            }
        }

    }
    xmlHttp.send(params);
  }
}

function handleServerResponse(){
  if(xmlHttp.readyState==4){
    if(xmlHttp.status==200){
      xmlResponse = xmlHttp.responseXML;
      xmlDocumentElement = xmlResponse.documentElement;
      message = xmlDocumentElement.firstChild.data;
    }
  }
}

navigator.getUserMedia =  navigator.getUserMedia ||
                          navigator.webkitGetUserMedia ||
                          navigator.mozGetUserMedia ||
                          navigator.oGetUserMedia ||
                          navigator.msGetUserMedia;
if (video)
{
  if(navigator.getUserMedia)
  {
    navigator.getUserMedia({video:true}, streamWebCam, throwError);
  }
}

function streamWebCam (stream) {
  video.src = window.URL.createObjectURL(stream);
  video.play();
}

function throwError (e) {
  alert(e.name);
  process("novideo");
}

function snap ()
{
  canvas.width = video.clientWidth;
  canvas.height = video.clientHeight;
  context.drawImage(video, 0, 0, 320, 240);
  oi = canvas.toDataURL();
  // alert(oi);
  process(oi);
  // document.getElementById('preimage').src = "dest.png?" + new Date().getTime();
  // document.getElementById('preimage').setAttribut('src', 'dest.png')
  // canvas.width = window.innerWidth;
  // canvas.height = window.innerHeight;
}

function save_pic()
{
  io = "save_pic";
  process(io);
}

function change_effect()
{
    document.getElementById("effect").src = "upload/alpha/42_trans.png";
}

function change_next_effect()
{
  process("next_effect");
}

function logout()
{
  window.location = "http://localhost:8080/logout.php";
}

function processPhoto(data)
{
  var params = "photo=" + data;
  if(xmlHttp.readyState==0 || xmlHttp.readyState==4)
  {
    xmlHttp.open("POST", "log.php",true);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.onreadystatechange = function() {
        if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
        {
            if (data != "next_effect" && data != "save_pic" && data != "novideo")
              document.getElementById('preimage').src = "dest.png?" + new Date().getTime();
        }

    }
    xmlHttp.send(params);
  }
}

function allmyphotos()
{
  window.location = "http://localhost:8080/photos.php?who=me";
}

function gomain()
{
  window.location = "http://localhost:8080/camagru.php";
}

function gotophoto(url)
{
  window.location = "http://localhost:8080/photos.php?one=" + url;
}

function allphotos()
{
  window.location = "http://localhost:8080/photos.php?who=everyone";
}

function likephoto(path)
{
  process(path);
}
